﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singleton
{
    public class Employee
    {
        public static Employee employee = null;        
        private static object lockObject = new object();   
        private Employee()
        {

        }

        public static Employee getInstance()
        {
            if (employee == null)
            {
                lock (lockObject)
                {
                    if (employee == null)
                    {
                        employee = new Employee();
                    }
                }
            }
            return employee;
        }
    }
}
